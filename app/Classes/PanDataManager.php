<?php

namespace App\Classes;

use App\Models\PanData;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Pagination\Paginator;
use App\User;

class PanDataManager
{
	protected $PanData;

	public function __construct(PanData $PanData) {
        $this->panData = $PanData;
        
    }

    public function getAllDetails()
    {
    	try {
    		$panList = $this->panData->getAllDetails();

    		if($panList) {
    			return $panList;
    		}
    		return null;
    	} catch(\Exception $exception) {
    		Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
    		Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
    	}
    }

    public function getApplication($pan_id)
    {
        try {
            $panDetails = $this->panData->getApplication($pan_id);

            if($panDetails) {
                return $panDetails;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function getTodaySuccess()
    {
        try {
            $panList = $this->panData->getTodaySuccess();

            if($panList) {
                return $panList;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function suspendUser($inputData)
    {
        try {
            $setDefaultBank = $this->user->suspendUser($inputData);

            if($setDefaultBank) {
                $result['status'] = true;
                $result['message'] = trans('user.user_suspended_success');
                return prepareRes($result, 200);
            }
            $result['status'] = false;
            $result['message'] = trans('user.user_suspended_failed');
            return prepareRes($result, 400);
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['suspendUser' => $exception->getMessage()]);
            Log::error('userManager Error', ['suspendUser' => $exception->getMessage()]);
            $result['status'] = false;
            $result['message'] = trans('user.user_suspended_failed');
            return prepareRes($result, 500);
        }
    }
}