<?php

namespace App\Classes;

use App\Models\PanData;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use App\User;

class dashboardManager
{
    protected $PanData;

    public function __construct(PanData $PanData) {
        $this->panData = $PanData;
        
    }

public function getDashboardData()
    {
        try {

            
            $dashboard['today_application'] = DB::table('tbl_pandata')->whereDate('timestamp', '=', Carbon::today())->count();
            $dashboard['total_application'] = DB::table('tbl_pandata')->count();
                                   
            
            if($dashboard) {
                return $dashboard;
            }

            return null;

        } catch(Exception $exception) {
            dd($exception->getMessage(), 2);

            Log::critical('dashboardManager Error', ['getDashboardUserData' => $exception->getMessage()]);
            Log::error('dashboardManager Error', ['getDashboardUserData' => $exception->getMessage()]);
            return null;
        }
    }

}