<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\order;
use App\Classes\Constant\AccountStatus;
use App\Classes\logActionManager;

class PanData
{

    protected $connection = 'mysql';
    public $timestamps = false;
    protected $table = 'tbl_pandata';

    // public $timezone = 'Asia/Kolkata';

    public function getAllDetails()
    {
        try {
            $panList = DB::table('tbl_pandata')->orderBy('timestamp', 'desc')->get();


            if($panList) {
                return $panList;
            }
            return null;
        } catch(QueryException $queryException) {
            Log::error('User Model', ['getAllUsers' => $queryException->getMessage()]);
            return null;
        }
    }
    public function getTodaySuccess()
    {
        try {
            $panList = DB::table('tbl_pandata')
                        ->join('tbl_transactions', 'tbl_transactions.pan_id','=','tbl_pandata.pan_id')
                        ->where('status','=','Captured')
                        ->orderBy('created_at', 'desc')
                        ->get();
                        
                        
                
            if($panList) {
                return $panList;
            }
            return null;
        } catch(QueryException $queryException) {
            Log::error('User Model', ['getAllUsers' => $queryException->getMessage()]);
            return null;
        }
    }

    public function getApplication($pan_id)
    {
        try {
            $panDetails = DB::table('tbl_pandata')->where('pan_id', $pan_id)->first();

            if($panDetails) {
                return $panDetails;
            }
            return null;
        } catch(QueryException $queryException) {
            Log::error('User Model', ['getAllUsers' => $queryException->getMessage()]);
            return null;
        }
    }

}