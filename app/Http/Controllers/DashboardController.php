<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\PanDataManager;
use App\Classes\dashboardManager;



class DashboardController extends Controller
{

	protected $PanDataManager;
    protected $dashboardManager;


	public function __construct(PanDataManager $PanDataManager, DashboardManager $dashboardManager)
    {
        $this->PanDataManager = $PanDataManager;
        $this->dashboardManager = $dashboardManager;
    }

    public function getDashboard(){

        $dashboardData = $this->dashboardManager->getDashboardData();

                           
    	 return view('dashboard')->with($dashboardData);
    }

    public function getAllData(Request $request){

    	$panDetails = $this->PanDataManager->getAllDetails();

    	return view('dashboard.list', compact('panDetails'));
    }

    public function getTodaySuccess(Request $request){

        $panDetails = $this->PanDataManager->getTodaySuccess();

        return view('dashboard.today-success-list', compact('panDetails'));
    }

    public function getApplication($pan_id){

    	$panData = $this->PanDataManager->getApplication($pan_id);
    	//dd($panData);
    	return view('dashboard.view', compact('panData'));
    }
}
