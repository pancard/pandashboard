<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','DashboardController@getDashboard');

Route::get('/view', function () { return view('dashboard.view');});

Route::get('dashboard', 'DashboardController@getDashboard');

Route::get('list', 'DashboardController@getAllData');
Route::get('today-success', 'DashboardController@getTodaySuccess');
Route::get('/view/{pan_id}', 'DashboardController@getApplication');