<!-- Side Nav START -->
<div class="side-nav">
    <div class="side-nav-inner">
        <ul class="side-nav-menu scrollable">

            <li class="nav-item {{ request()->is('dashboard') ? 'active' : '' }}">
                <a class="dropdown-toggle" href="{{ URL::to('/dashboard') }}">
                    <span class="icon-holder">
                        <i class="anticon anticon-dashboard"></i>
                    </span>
                    <span class="title">Dashboard</span>
                    <span class="arrow">
                        <i class="arrow-icon"></i>
                    </span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('users') ? 'active' : '' }} {{ request()->is('user/details/*') ? 'active' : '' }}">
                <a class="dropdown-toggle" href="{{ URL::to('/list') }}">
                    <span class="icon-holder">
                        <i class="anticon anticon-user"></i>
                    </span>
                    <span class="title">All Applications</span>
                    <span class="arrow">
                        <i class="arrow-icon"></i>
                    </span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('kyc-docs') ? 'active' : '' }}">
                <a class="dropdown-toggle" href="{{ URL::to('/today-success') }}">
                    <span class="icon-holder">
                        <i class="anticon anticon-file-search"></i>
                    </span>
                    <span class="title">Today Success</span>
                    <span class="arrow">
                        <i class="arrow-icon"></i>
                    </span>
                </a>
            </li>

            
            <li class="nav-item {{ request()->is('orders') ? 'active' : '' }}">
                <a class="dropdown-toggle" href="{{ URL::to('/orders') }}">
                    <span class="icon-holder">
                        <i class="anticon anticon-shopping-cart"></i>
                    </span>
                    <span class="title">Orders</span>
                    <span class="arrow">
                        <i class="arrow-icon"></i>
                    </span>
                </a>
            </li>       

        </ul>
    </div>
</div>
<!-- Side Nav END -->