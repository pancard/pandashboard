<div class="header">
    <div class="logo logo-dark">
        <a href="{{ URL::to('dashboard') }}">
            <!-- <img src="{{ URL::asset('images/logo.png') }}" alt="C2C Logo" style="margin-top: 23px;"> -->
            <img src="{{ URL::asset('images/logo.png') }}" alt="C2C Logo" style="padding: 10px">
            <!-- <img class="logo-fold" src="{{ URL::to('images/logo-fold.png') }}" alt="C2C Logo" style="margin-top: 23px; margin-left: 20px;"> -->
            <img class="logo-fold" src="{{ URL::to('images/logo-fold.png') }}" alt="C2C Logo" style="padding: 5px; height: 45px; width: 65px; margin: 6px;">
        </a>
    </div>
    <div class="logo logo-white">
        <a href="{{ URL::to('dashboard') }}">
            <img src="{{ URL::asset('images/logo.png') }}" alt="Logo" style="padding: 10px">
            <!-- <img class="logo-fold" src="{{ URL::asset('images/logo-fold.png') }}" alt="Logo" style="margin-top: 23px; margin-left: 20px;"> -->
            <img class="logo-fold" src="{{ URL::asset('images/logo-fold.png') }}" alt="Logo" style="padding: 5px; height: 45px; width: 65px; margin: 6px;">
        </a>
    </div>
    <div class="nav-wrap">
        <ul class="nav-left">
            <li class="desktop-toggle">
                <a href="javascript:void(0);">
                    <i class="anticon"></i>
                </a>
            </li>
            <li class="mobile-toggle">
                <a href="javascript:void(0);">
                    <i class="anticon"></i>
                </a>
            </li>
        </ul>
        <ul class="nav-right">
            <li class="dropdown dropdown-animated scale-left">
                <div class="pointer" data-toggle="dropdown">
                    <div class="avatar avatar-icon avatar-md avatar-primary">
                        <i class="anticon anticon-user"></i>
                    </div>
                </div>
                <div class="p-b-15 p-t-20 dropdown-menu pop-profile">
                    <div class="p-h-20 p-b-15 border-bottom">
                        <div class="d-flex m-r-50">
                            <div class="m-l-10">
                                <p class="m-b-0 text-dark font-weight-semibold"></p>
                                <p class="m-b-0 opacity-07"></p>
                            </div>
                        </div>
                    </div>
                    <a data-toggle="modal" data-target="#change_pass_modal" class="dropdown-item d-block p-h-15 p-v-15 border-bottom" style="cursor: pointer;">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <i class="anticon opacity-04 font-size-16 anticon-setting"></i>
                                <span class="m-l-10">Change Password</span>
                            </div>
                        </div>
                    </a>
                    <a href="{{ URL::to('logout') }}" class="dropdown-item d-block p-h-15 p-v-10">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <i class="anticon opacity-04 font-size-16 anticon-logout"></i>
                                <span class="m-l-10">Logout</span>
                            </div>
                            <i class="anticon font-size-10 anticon-right"></i>
                        </div>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</div>

<div id="change_pass_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="change_pass_modal"  data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="max-width: 400px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #ffffff; display: block;">
                <button id="close" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <input type="hidden" id="order_id"/>
            </div>
            <div class="modal-body" style="padding: 5px 0px 16px 16px;">
                <form id="changePassword" name="changePassword">
                    <div class="col-12">
                        <h4 class="modal-title" style="color:#554075;font-weight: 500;margin-bottom: 10px;">Change Password </h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group" style="margin-bottom: 1rem;">
                                    <input id="current_password" name="current_password" type="password" class="form-control" autocomplete="off" placeholder="Enter Current Password">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group" style="margin-bottom: 1rem;">
                                    <input id="new_password" name="new_password" type="password" class="form-control" autocomplete="off" placeholder="Enter New Password">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group" style="margin-bottom: 1rem;">
                                    <input id="new_password_conf" name="new_password_conf"  type="password" class="form-control" autocomplete="off" placeholder="Re-enter new password">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group text-center" style="margin-bottom: 1rem; margin-top: 1rem;">
                                    <button class="btn btn-primary m-r-5" style="background: #554075;">
                                        <span>Change Password</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
