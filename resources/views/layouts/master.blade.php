<!doctype html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="csrf-token" content="{!! csrf_token() !!}">
	    <title> Online Panseva | @yield('title')</title>

	    <!-- Favicon -->
	    <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">

	    <!-- page css -->
		<link href="{{ URL::asset('assets/vendors/select2/select2.css') }}" rel="stylesheet">

	    <!-- Core css -->
	    <link href="{{URL::asset('assets/css/app.min.css')}}" rel="stylesheet">
	    <link href="{{URL::asset('css/snackbar.min.css')}}" rel="stylesheet">
	    <link href="{{URL::asset('css/custom.css')}}" rel="stylesheet">

	    <!-- Datepicker css -->
		<link href="{{URL::asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

		<!-- Alert css -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  	</head>

  	<body>
    	<div class="app">
	  		<div class="layout">
	    		<!-- Header START -->
	    		@include ('layouts.include.header')
	            <!-- Header END -->

	            <!-- Side Nav START -->
	            @include ('layouts.include.left-sidebar')
	            <!-- Side Nav END -->

		  		<!-- Page Container START -->
		        <div class="page-container">
		        	<!-- Content Wrapper START -->
		        	<div class="main-content">
	            		@yield('content')
		            </div>

			  		<!-- Footer START -->
			        <footer class="footer">
			            <div class="footer-content">
			                <p class="m-b-0">Copyright © {{\Carbon\Carbon::today()->format('Y')}} Credit2Bank. All rights reserved.</p>
			            </div>
			        </footer>
			        <!-- Footer END -->
		        </div>
		        <!-- Page Container END -->
        	</div>
        </div>



		<!-- Core Vendors JS -->
	    <script src="{{URL::asset('assets/js/vendors.min.js')}}"></script>

	    <!-- page js -->
	    <script src="{{URL::asset('assets/vendors/chartjs/Chart.min.js')}}"></script>
		<script src="{{ URL::asset('assets/vendors/select2/select2.min.js') }}"></script>

	    <!-- Core JS -->
	    <script src="{{URL::asset('assets/js/app.min.js')}}"></script>

	    <!-- Alert JS -->
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

		<!-- Datepicker JS -->
		<script src="{{URL::asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
		<!-- Helper JS -->
		<script src="{{URL::asset('js/helpers.js')}}"></script>
		<script src="{{URL::asset('js/snackbar.min.js')}}"></script>
		<script src="{{URL::asset('js/download2.js')}}"></script>

		<!-- Moment JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
		<script src="{{URL::asset('assets/js/moment-timezone.js')}}"></script>
		<script src="{{URL::asset('assets/js/moment-timezone-with-data.js')}}"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

            @yield('customJs')

  	</body>
</html>
