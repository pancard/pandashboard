@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')
	
    
<div class="row">  
    <div class="col-8">
        <h4>1. PERSONAL INFORMATION</h4>
        <div class="form-group mb-0 row">
            <div class="col-sm-3">
                <label class="mt-5px my-form-label text-blue">Title</label>
                <input class="form-control ptxb-0505" value="{{$panData->applicant_name_title}}" disabled="">
            </div>
            <div class="col-sm-3">
                <label class="mt-5px my-form-label text-blue">First Name</label>
                <input class="form-control ptxb-0505" value="{{$panData->applicant_first_name}}" disabled="">
            </div>
            <div class="col-sm-3">
                <label class="mt-5px my-form-label text-blue">Middle Name</label>
                <input class="form-control ptxb-0505" value="{{$panData->applicant_middle_name}}" disabled="">
            </div>
            <div class="col-sm-3">
                <label class="mt-5px my-form-label text-blue">Surname</label>
                <input class="form-control ptxb-0505" value="{{$panData->applicant_last_name}}" disabled="">
            </div>
        </div>
        <p class="text-blue">
            <small><b>(Please Enter Full Name, Initials not allowed)</b></small>
        </p>
        <div class="form-group mb-0 row">
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Father Name<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->father_first_name}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Father Middle Name</label>
                <input class="form-control ptxb-0505" value="{{$panData->father_middle_name}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Father Surname</label>
                <input class="form-control ptxb-0505" value="{{$panData->father_last_name}}" disabled="">
            </div>
        </div>
        <div class="form-group mb-0 row">
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Mother Name(Optional)</label>
                <input class="form-control ptxb-0505" value="{{$panData->mother_first_name}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Mother Middle Name</label>
                <input class="form-control ptxb-0505" value="{{$panData->mother_middle_name}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Mother Surname</label>
                <input class="form-control ptxb-0505" value="{{$panData->mother_last_name}}" disabled="">
            </div>
        </div>
        <p class="text-blue">
            <small><b>(Please Enter Full Name, Initials not allowed)</b></small>
        </p>
        <div class="form-group mb-0 row">
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Mobile No.<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->applicant_mobile_no}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Email ID<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->applicant_email_id}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">BirthDate<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->date_of_birth}}" disabled="">
            </div>
        </div>
        <div class="form-group mb-0 row">
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Aadhaar/ Enrollment No.<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->applicant_aadhaar_no}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Name on Aadhaar<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->name_of_aadhaar}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Aadhaar Proof<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->aadhaar_proof}}" disabled="">
            </div>
        </div>
        <div class="form-group mb-0 row">
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Gender<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->gender}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Source of Income<span style="color:red">*</span></label>
                <input class="form-control ptxb-0505" value="{{$panData->source_of_income}}" disabled="">
            </div>
            <div class="col-sm-4">
                <label class="mt-5px my-form-label text-blue">Communication Address</label>
                <input class="form-control ptxb-0505" value="{{$panData->communication_address}}" disabled="">
            </div>
        </div>
        <hr>
        <h4>2. RESIDENTIAL ADDRESS OF APPLICANT</h4>
        <div class="col bg-light-gray pb-10px">
            <div class="row">
                <div class="col-12">
                    <div class="form-group mb-0 row">
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">House No./ Building / Village<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->house_no}}" disabled="">
                        </div>
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">Area/ Road/ P.O.<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->area_street}}" disabled="">
                        </div>
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">State<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->state_union}}" disabled="">
                        </div>
                    </div>
                    <div class="form-group mb-0 row">
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">City/ Town/ District<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->city_district}}" disabled="">
                        </div>
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">Pin Code<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->pin_zip}}" disabled="">
                        </div>
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">Country<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->country}}" disabled="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h4>3. DOCUMENT SUBMITTED AS PROOF</h4>

        <div class="col bg-light-gray pb-10px">
            <div class="row">
                <div class="col-12">
                    <div class="form-group mb-0 row">
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">Identity Proof<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->id_proof}}" disabled="">
                        </div>
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">DOB Proof<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->dob_proof}}" disabled="">
                        </div>
                        <div class="col-sm-4">
                            <label class="mt-5px my-form-label text-blue">Address Proof<span style="color:red">*</span></label>
                            <input class="form-control ptxb-0505" value="{{$panData->addr_proof}}" disabled="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>                

@stop

@section('customJs')

    <!-- page js -->
    <script src="{{URL::asset('assets/vendors/chartjs/Chart.min.js')}}"></script>
    {{--<script src="{{URL::asset('assets/js/pages/dashboard-default.js')}}"></script>--}}

@stop
