@extends('layouts.master')

@section('title', 'Users')

@section('content')
	<div class="page-header">
        <h2 class="header-title">User Management</h2>
        <div class="header-sub-title">
            <nav class="breadcrumb breadcrumb-dash">
                <a href="#" class="breadcrumb-item"><i class="anticon anticon-user m-r-5"></i>Users</a>
            </nav>
        </div>
    </div>

	<form action="#" type="POST">
		<div class="form-row align-items-center">
			<div class="col-auto">
				<input class="form-control amber-border" type="text" placeholder="Search Email/Mob No." aria-label="Search" id="search_q">
			</div>

			<div class="col-auto">
				<select class="form-control" id="account_status">
					<option class="dropdown-item" value="" selected>Select Status</option>
					<option class="dropdown-item" value="NEW">Success</option>
					<option class="dropdown-item" value="FULL_APPROVE">Pending</option>
					<option class="dropdown-item" value="ON_HOLD">Complate</option>
					<option class="dropdown-item" value="SUSPENDED">Suspended</option>
				</select>
			</div>

			<div class="col-auto">
				<!-- <button class="btn btn-primary" id="search_user">Submit</button> -->
				<a href="javascript:filterUser('1')" class="btn btn-primary">Submit</a>
				<button class="btn btn-primary btn-tone m-r-5" onclick="window.location.reload();">Clear</button>
			</div>
		</div>
	</form>

	<br><br>

	<div class="table-responsive">
		<div class="card">
			<div class="card-body">
				<div id="filterResponse">
					<table class="table table-hover">
				        <thead>
				            <tr>
				                <th scope="col">No.</th>
				                <th scope="col">Application No.</th>
				                <th scope="col">Full Name</th>
				                <th scope="col">Email</th>
                                <th scope="col">Aadhar No</th>
				                <th scope="col">Mobile No</th>
                                <th scope="col">Status</th>
                                <th scope="col">Date</th>
                                <th scope="col">Action</th>
				            </tr>
				        </thead>
				        <tbody>
                        @if(isset($panDetails) && !empty($panDetails))
                            @foreach($panDetails as $key => $data)				        
    				            <tr>
    				                <td>{{ $data->no ? $data->no : '-' }}</td>    				            
                                    <td>{{ $data->pan_id ? $data->pan_id : '-' }}</td>
                                    <td>{{ $data->applicant_first_name }} {{ $data->applicant_last_name }}</td>
                                    <td>{{ $data->applicant_email_id ? $data->applicant_email_id : '-' }}</td>
                                    <td>{{ $data->applicant_aadhaar_no ? $data->applicant_aadhaar_no : '-' }}</td>
                                    <td>{{ $data->applicant_mobile_no ? $data->applicant_mobile_no : '-' }}</td>
                                    <td>Wait</td>
                                    <td>{{ $data->timestamp ? $data->timestamp : '-' }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-primary btn-tone m-r-5 m-t-5" href="{{ URL::to('/view/'.$data->pan_id) }}" data-toggle="tooltip" target="_blank" title="View Details"><i class="anticon anticon-contacts"></i></a>
                                        <a class="btn btn-xs btn-primary btn-tone m-r-5 m-t-5" href="" data-toggle="tooltip" title="View Pan"><i class="anticon anticon-edit"></i></a>
                                    </td>
    				            </tr>
                            @endforeach
                         @else
                              <tr>
                                <td colspan="9" align="center">User Does Not Exist!</td>
                              </tr>
                        @endif				            			               			             
				        </tbody>
				    </table>
                            
			</div>
		</div>
	</div>

@stop
